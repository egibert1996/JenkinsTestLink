package conjuntProvesGoogle;

//import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
//import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.support.ui.Select;

public class NavegacionGoogle {
	private WebDriver driver;
	private String baseUrl;
	// private boolean acceptNextAlert = true;
	// private StringBuffer verificationErrors = new StringBuffer();

	@Test
	public void navegacionGoogle() throws Exception {

		System.setProperty("webdriver.gecko.driver", "C:\\geckodriver\\geckodriver.exe");
		driver = new FirefoxDriver();
		baseUrl = " https://www.google.es/";

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get(baseUrl + "/?gws_rd=ssl");
	    driver.findElement(By.linkText("Im�genes")).click();
	    driver.findElement(By.id("lst-ib")).clear();
	    driver.findElement(By.id("lst-ib")).sendKeys("selenium");
	    driver.findElement(By.id("_fZl")).click();
	    driver.findElement(By.id("hdtb-tls")).click();
	    driver.findElement(By.cssSelector("span.mn-dwn-arw")).click();
	    driver.findElement(By.linkText("Grande")).click();
	    driver.close();
	}
}
