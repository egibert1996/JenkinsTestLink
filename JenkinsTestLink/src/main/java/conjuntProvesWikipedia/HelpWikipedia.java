package conjuntProvesWikipedia;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class HelpWikipedia {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@Test
	public void helpWikipedia() throws Exception {

		System.setProperty("webdriver.gecko.driver", "C:\\geckodriver\\geckodriver.exe");
		driver = new FirefoxDriver();
		baseUrl = "https://en.wikipedia.org/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get(baseUrl);
		driver.findElement(By.linkText("Help")).click();
		driver.findElement(By.name("search")).click();
		driver.findElement(By.name("search")).clear();
		driver.findElement(By.name("search")).sendKeys("help");
		driver.findElement(By.name("fulltext")).click();
		driver.findElement(By.id("mw-search-ns8")).click();
		driver.findElement(By.id("mw-search-ns11")).click();
		driver.findElement(By.cssSelector("button.oo-ui-inputWidget-input.oo-ui-buttonElement-button")).click();
		driver.findElement(By.id("ooui-1")).clear();
		driver.findElement(By.id("ooui-1")).sendKeys("Islands of Adventure");
		driver.findElement(By.xpath(".//*[@id='mw-search-top-table']/div[1]/div/div/span/span/button")).click();
		driver.close();
	}
}
