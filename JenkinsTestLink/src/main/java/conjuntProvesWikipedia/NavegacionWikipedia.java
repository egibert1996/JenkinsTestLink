package conjuntProvesWikipedia;

import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Test;

public class NavegacionWikipedia {
	private WebDriver driver;
	private String baseUrl;
	//private boolean acceptNextAlert = true;
	//private StringBuffer verificationErrors = new StringBuffer();

	@Test
	public void navegacionWikipedia() throws Exception {
		System.setProperty("webdriver.gecko.driver", "C:\\geckodriver\\geckodriver.exe");
		driver = new FirefoxDriver();
		baseUrl = "https://en.wikipedia.org/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		// Selenium Test
		driver.get(baseUrl);
		driver.findElement(By.id("searchInput")).clear();
		driver.findElement(By.id("searchInput")).sendKeys("island");
		driver.findElement(By.id("searchButton")).click();
		driver.findElement(By.linkText("water")).click();
		driver.findElement(By.id("searchInput")).clear();
		driver.findElement(By.id("searchInput")).sendKeys("Family Guy");
		driver.findElement(By.id("searchButton")).click();
		driver.findElement(By.linkText("Seth MacFarlane")).click();
		driver.close();

		System.out.println("El test de Edgar si funciona.");

	}

}
  